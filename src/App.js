import React, { useRef, useEffect } from "react";

// Widgets
import Bookmarks from '@arcgis/core/widgets/Bookmarks';
import Expand from '@arcgis/core/widgets/Expand';
import LayerList from "@arcgis/core/widgets/LayerList";
import Feature from "@arcgis/core/widgets/Feature";

import * as promiseUtils from "@arcgis/core/core/promiseUtils";


// Layers
import FeatureLayer from "@arcgis/core/layers/FeatureLayer";


// Map
import MapView from "@arcgis/core/views/MapView";
import WebMap from "@arcgis/core/WebMap";

import "./App.css"; 

function App() {

  const mapDiv = useRef(null);

  useEffect(() => {
    if (mapDiv.current) {
      /**
       * Initialize application
       */

      // Layers
      let vtlLayer = new FeatureLayer({
        // URL to the style of vector tiles
        url: "https://ntia-sandbox-dev.esriemcs.com/hosted/rest/services/NBAM_Omnibus_v5/MapServer/2",
          outFields: ["*"]
      });

      // webmap.addMany([vtlLayer]);

      const webmap = new WebMap({
        portalItem: {
          id: "67372ff42cd145319639a99152b15bc3"
        },
        layers: [vtlLayer]

      });

      const view = new MapView({
        container: mapDiv.current,
        map: webmap
      });

      const bookmarks = new Bookmarks({
        view,
        // allows bookmarks to be added, edited, or deleted
        editingEnabled: true
      });

      const bkExpand = new Expand({
        view,
        content: bookmarks,
        expanded: false
      });

      // Add the widget to the top-right corner of the view
      view.ui.add(bkExpand, "top-right");

      // Add Layerlist Widget with options

      // Creates actions in the LayerList.

      function defineActions(event) {
        // The event object contains an item property.
        // is is a ListItem referencing the associated layer
        // and other properties. You can control the visibility of the
        // item, its title, and actions using this object.

        const item = event.item;

        console.log('item test: ', item);
          // An array of objects defining actions to place in the LayerList.
          // By making this array two-dimensional, you can separate similar
          // actions into separate groups with a breaking line.

          item.actionsSections = [
            [
              {
                title: "Go to full extent",
                className: "esri-icon-zoom-out-fixed",
                id: "full-extent"
              },
              {
                title: "Layer information",
                className: "esri-icon-description",
                id: "information"
              }
            ],
            [
              {
                title: "Increase opacity",
                className: "esri-icon-up",
                id: "increase-opacity"
              },
              {
                title: "Decrease opacity",
                className: "esri-icon-down",
                id: "decrease-opacity"
              }
            ]
          ];
      }

      const layerList = new LayerList({
        view: view,
        // executes for each ListItem in the LayerList
        listItemCreatedFunction: defineActions
      });

      // Event listener that fires each time an action is triggered

      layerList.on("trigger-action", (event) => {
        // The layer visible in the view at the time of the trigger.
        const visibleLayer = vtlLayer; // USALayer.visible ? USALayer : censusLayer;

        // Capture the action id.
        const id = event.action.id;

        if (id === "full-extent") {
          // if the full-extent action is triggered then navigate
          // to the full extent of the visible layer
          view.goTo(visibleLayer.fullExtent).catch((error) => {
            if (error.name !== "AbortError") {
              console.error(error);
            }
          });
        } else if (id === "information") {
          // if the information action is triggered, then
          // open the item details page of the service layer
          window.open(visibleLayer.url);
        } else if (id === "increase-opacity") {
          // if the increase-opacity action is triggered, then
          // increase the opacity of the GroupLayer by 0.25
          
          if (visibleLayer.opacity < 1) {
            visibleLayer.opacity += 0.25;
          }
        } else if (id === "decrease-opacity") {
          // if the decrease-opacity action is triggered, then
          // decrease the opacity of the GroupLayer by 0.25
          if (visibleLayer.opacity > 0) {
            visibleLayer.opacity -= 0.25;
          }
        }
      });

      // Add widget to the top right corner of the view
      view.ui.add(layerList, "top-left");

      // Create a default graphic for when the application starts
      const graphic = {
        popupTemplate: {
          content: "Mouse over features to show details..."
        }
      };

      // Provide graphic to a new instance of a Feature widget
      /*
      const feature = new Feature({
        graphic: graphic,
        map: view.map,
        spatialReference: view.spatialReference
      });

      view.ui.add(feature, "bottom-left");
      */

      // bonus - how many bookmarks in the webmap?
      webmap.when(() => {
        if (webmap.bookmarks && webmap.bookmarks.length) {
          console.log("Bookmarks: ", webmap.bookmarks.length);
        } else {
          console.log("No bookmarks in this webmap.");
        }

        // Create a default graphic for when the application starts
        const graphic = {
          popupTemplate: {
            content: "Mouse over features to show details..."
          }
        };

        // Provide graphic to a new instance of a Feature widget
        const feature = new Feature({
          container: "feature-node",
          graphic: graphic,
          map: webmap,
          spatialReference: view.spatialReference
        });

        view.whenLayerView(vtlLayer).then((layerView) => {
          console.log('layerView: ', layerView);
          let highlight;
          // listen for the pointer-move event on the View
          view.on("pointer-move", (event) => {
            // Perform a hitTest on the View
            view.hitTest(event).then((event) => {
              
              // Make sure graphic has a popupTemplate
              const results = event.results.filter((result) => {
                return "<p>Blah</p>";
              });
              const result = results[0];
              highlight && highlight.remove();
              // Update the graphic of the Feature widget
              // on pointer-move with the result
              if (result) {
                feature.graphic = result.graphic;
                highlight = layerView.highlight(result.graphic);
              } else {
                feature.graphic = graphic;
              }
            });
          });
        });

      });

    }
  }, []);

  return <div className="mapDiv" ref={mapDiv}>

      <div id="feature-node" class="esri-widget"></div>

  </div>;
}

export default App;
